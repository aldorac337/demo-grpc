package main

import (
	"fmt"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"
	"grpc/api"
	"log"
	"net"
)

func main() {
	lis, err := net.Listen("tcp", fmt.Sprintf(":%d", 7777))
	 if err != nil {
	 	log.Fatalf("failed to listen: %v", err)
	 }

	//create server instance
	s := api.Server{}


	// Create the TLD credentials
	creds, err := credentials.NewServerTLSFromFile("cert/server.crt","cert/server.key")
	if err != nil {
		log.Fatalf("could not load TLD keys: %s", err)
	}

	// Create an array of gRPC options with the credentials
	opts := []grpc.ServerOption{grpc.Creds(creds)}

	// attach the Ping service to the server
	grpcServer := grpc.NewServer(opts...)

	api.RegisterPingServer(grpcServer, &s)

	if err := grpcServer.Serve(lis); err != nil {
		log.Fatalf("failed to serve: %s", err)
	}

}