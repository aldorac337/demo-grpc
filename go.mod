module grpc

require (
	github.com/golang/protobuf v1.2.0
	github.com/googleapis/googleapis v0.0.0-20181008181953-7b34e5a3ed10 // indirect
	github.com/grpc-ecosystem/grpc-gateway v1.5.1
	github.com/protocolbuffers/protobuf v3.6.1+incompatible // indirect
	gitlab.com/pantomath-io/demo-grpc v0.0.0-20171010145848-6dd27f9b2ed4
	golang.org/x/net v0.0.0-20181005035420-146acd28ed58
	google.golang.org/grpc v1.15.0
)
